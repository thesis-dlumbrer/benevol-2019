\documentclass[conference]{IEEEtran}
\IEEEoverridecommandlockouts
% The preceding line is only needed to identify funding in the first footnote. If that is unneeded, please comment it out.
\usepackage{cite}
\usepackage{amsmath,amssymb,amsfonts}
\usepackage{algorithmic}
\usepackage{graphicx}
\usepackage{textcomp}
\usepackage{xcolor}
\documentclass{scrreprt}
\usepackage{color}
\definecolor{editorGray}{rgb}{0.95, 0.95, 0.95}
\definecolor{editorOcher}{rgb}{1, 0.5, 0} % #FF7F00 -> rgb(239, 169, 0)
\definecolor{editorGreen}{rgb}{0, 0.5, 0} % #007C00 -> rgb(0, 124, 0)
\usepackage{upquote}
\usepackage{listings}
\lstdefinelanguage{JavaScript}{
  morekeywords={typeof, new, true, false, catch, function, return, null, catch, switch, var, if, in, while, do, else, case, break},
  morecomment=[s]{/*}{*/},
  morecomment=[l]//,
  morestring=[b]",
  morestring=[b]'
}

\lstdefinelanguage{HTML5}{
        language=html,
        sensitive=true, 
        alsoletter={<>=-},
        otherkeywords={
        % HTML tags
        <html>, <head>, <title>, </title>, <meta, />, </head>, <body>,
        <canvas, \/canvas>, <script>, </script>, </body>, </html>, <!, html>, <style>, </style>, ><
        },  
        ndkeywords={
        % General
        =,
        % HTML attributes
        charset=, id=, width=, height=,
        % CSS properties
        border:, transform:, -moz-transform:, transition-duration:, transition-property:, transition-timing-function:
        },  
        morecomment=[s]{<!--}{-->},
        tag=[s]
}

\lstset{%
    % Basic design
    backgroundcolor=\color{editorGray},
    basicstyle={\fontsize{6}{7}\ttfamily},  
    %basicstyle=\fontsize{11}{13}\selectfont\ttfamily
    frame=l,
    % Line numbers
    xleftmargin={0.75cm},
    numbers=left,
    stepnumber=1,
    firstnumber=1,
    numberfirstline=true,
    % Code design   
    keywordstyle=\color{blue}\bfseries,
    commentstyle=\color{darkgray}\ttfamily,
    ndkeywordstyle=\color{editorGreen}\bfseries,
    stringstyle=\color{editorOcher},
    % Code
    language=HTML5,
    alsolanguage=JavaScript,
    alsodigit={.:;},
    tabsize=2,
    showtabs=false,
    showspaces=false,
    showstringspaces=false,
    extendedchars=true,
    breaklines=true,        
    % Support for German umlauts
    literate=%
    {Ö}{{\"O}}1
    {Ä}{{\"A}}1
    {Ü}{{\"U}}1
    {ß}{{\ss}}1
    {ü}{{\"u}}1
    {ä}{{\"a}}1
    {ö}{{\"o}}1
}
\def\BibTeX{{\rm B\kern-.05em{\sc i\kern-.025em b}\kern-.08em
    T\kern-.1667em\lower.7ex\hbox{E}\kern-.125emX}}
\begin{document}

\title{Making CodeCity evolve\\
}

\author{\IEEEauthorblockN{David Moreno-Lumbreras}
\IEEEauthorblockA{\textit{Universidad Rey Juan Carlos} \\
\textit{Bitergia}\\
Madrid, Spain \\
dmoreno@bitergia.com}
\and
\IEEEauthorblockN{Jesús M. González-Barahona}
\IEEEauthorblockA{
\textit{Universidad Rey Juan Carlos}\\
Madrid, Spain \\
jgb@gsyc.es}
\and
\IEEEauthorblockN{Valerio Cosentino}
\IEEEauthorblockA{
\textit{Bitergia}\\
Madrid, Spain \\
valcos@bitergia.com}
}

\maketitle

\begin{abstract}
CodeCity is a software analysis tool with the goal of visualizing software systems as interactive, navigable 3D cities. They rely on the city metaphor, that uses the layout of the city in order to visualize different metrics about software systems. There were other implementations of CodeCity among the years but there are no modern options that contemplate the representation of time evolution of the city. We propose a solution to this time evolution, developing a web version of CodeCity that works with any device that has a web browser. Then, we analyze different projects and show the time evolution of their cities. Finally, we conclude with some needed features and drawbacks that are going to be considered in future works.
\end{abstract}

\begin{IEEEkeywords}
CodeCity, data visualization, virtual reality, web, 3D, Virtual Reality
\end{IEEEkeywords}

\section{Introduction}

Codecity \cite{ref_article3} is an approach of a 3D visualization which creates cities that look real, due to the combination of layouts, topologies, metric mappings applied at an appropriate level of granularity. It depicts object-oriented software systems as habitable\cite{ref_article4} cities that one can intuitively explore. Codecity settles on the city metaphor because it offers a clear notion of locality, thus supporting orientation, and features a structural complexity that cannot be oversimplified. Codecity represents classes as buildings located in quarters representing the packages where the classes are defined. The metrics that Codecity uses are the number of methods (NOM) mapped on the building's height and the number of attributes (NOA) on their base size, and for packages, the nesting level mapped on the quarter's color saturation:

\begin{figure}[!htb]
  \centering
  \includegraphics[width=8cm, keepaspectratio]{img/codecity_sample.png}
  \caption{Example of the Jmol Java package city analyzed with Codecity}
  \label{fig:codecity_example}
\end{figure}

One of the known limitations of this approach is that CodeCity is developed using an old framework (SmartTalk) and it does not have support for several devices. We rely on web development and we are going to solve this limitation developing a version of CodeCity using web technologies and open source software in order to make the tool more universal and every device that has a web browser could visualize these 3D cities. In the next sections, we detailed the development and the technologies used. 

On the other hand, in terms of interaction, we argue that this visualization type can get more value and better comprehension adding it a Virtual Reality environment, allowing the user to tour and navigate into the city.

Another of its limitations is that Codecity doesn't follow a fixed layout for its blocks/quarters, it means that every time a code city is generated, the layout and the position of a package could change. This would be an important drawback in terms of time evolution analysis because the city would change every single time that it's generated. We are going to solve that, making the city evolve among the time, increasing and decreasing the area and height of the buildings as the metrics represented changes among the time, fixing the position of the buildings, fixing the same position of each building while the time snapshot changes.

Moreover, Codecity is strongly bonded to analyze the program structure. We argue that the city metaphor can be more than just analyze code, like analyze the contributions, users, and other kinds of metrics related to the community. This is one of the future works (section \ref{future_works}) that is going to be analyzed.

\section{Related works}

We are going to separate the related works in two categories, one related to the implementations of the city metaphor and the other related to the time evolution implementations about treemap algorithms. Regarding the city metaphor implementations, Juraj Vincur et all. \cite{ref_vr_city} propose a Virtual Reality city for analyzing software, made it with non-web technologies, Steinbrückner and Lewerentz \cite{ref_vr_city_2} propose stable city layouts for evolving software systems, using a different layout than a treemap. Getaviz \cite{ref_getaviz} is another tool that uses the city metaphor in order to generate structural, behavioral, and evolutionary views of software systems for empirical evaluation. In terms of interaction with the city, one good example is CityVR \cite{ref_article5} that uses the same metrics as the original Codecity but adds interactions with a VR headset using the controllers and the sight direction.
Regarding the evolution of a treemap layout, Willy Scheibel et all. propose EvoCells \cite{ref_evocells}, a treemap layout algorithm for evolving tree data, using rectangular areas. Another field of treemap evolution research is the Voronoi treemaps, these treemaps have not rectangular areas, making the treemap more flexible in order to evolve the areas, Avneesh Sud et all. \cite{ref_voronoi} proposes a dynamic Voronoi treemap algorithm that explores this dynamic changes that can be used as an evolution of the treemap.

\section{Our approach: BabiaXR and Web CodeCity with time evolution}

\subsection{BabiaXR Visualizations}
There are different technologies based on WebGL that are focused on making 3D scenes in the browser. Specifically, we rely on A-Frame \cite{ref_aframe}, a web framework for building 3D and VR scenes for the web, for the visualization render engine, developing, thus a new version of CodeCity that works in any device that has a web browser and the needed standards included (the most important, WebVR), A-Frame is developed on top Three.js \footnote{https://threejs.org/}.

Before the development of CodeCity, we got used to making some usual 3D visualization in this web 3D environment. Starting with BabiaXR \footnote{https://github.com/babiaxr}, a GitLab/GitHub organization where the developing of the different visualizations are allocated. BabiaXR has the aim of aggregate different components, using A-Frame, that can create different types of chart in a modern browser, the web is a universal environment and any device that has a modern browser that supports WebVR can visualize the charts that the BabiaXR components produce, making it more universal and easy to use.
The first step of BabiaXR was to make the most common visualizations, the pie chart, the 3D and 2D bar chart, and the bubbles chart. The development of these visualizations is separated in some A-Frame components, \textit{geosimplebarchart} for 2D bar chart, \textit{geo3dbarchart} for 3D bar chart, \textit{geopiechart} for pie chart and \textit{geobubbleschart} for bubbles chart. Moreover, there are components related to data management, these components can query, filter data and add behavior to the visualization components. There are two components that query data, \textit{querier\_json} for querying JSON files and \textit{querier\_github} that query the GitHub API. The component \textit{filterdata} can filter the data retrieved from a querier component and the component \textit{vismapper} that maps the data filtered by a filterdata component to geometry attributes of the different charts, it "prepares" the data and save it in the entity that it is defined.

In the repository, there is a user guide where all the information and the steps in order to create a 3D and VR dashboard are defined.

One example of these charts is the one showed in \ref{fig:vis_babia}:

\begin{figure}[!htb]
  \centering
  \includegraphics[width=8cm, keepaspectratio]{img/babia_example.png}
  \caption{Usual visualizations of BabiaXR}
  \label{fig:vis_babia}
\end{figure}

One of the advantages of A-Frame is that extends HTML, so using just two sentences of HTML with a JSON data can generate a 3D and VR visualization in the web environment. For example, the next HTML code and JSON data will generate the figure \ref{fig:babia_3d_chart}:
\begin{lstlisting}
<a-scene background="color: #A8F3FF" id="AframeScene">
    ...
    <a-entity geo3dbarchart='legend: true; data: example.json' 
    position="-10 0 0" rotation="0 0 0"></a-entity>
    ...
</a-scene>
\end{lstlisting}
%[
%    basicstyle=\tiny, %or \small or \footnotesize etc.
%]
\begin{lstlisting}
[
{"key":"David","key2":"2019","size":9},
{"key":"Pete","key2":"2011","size":8},
...]
\end{lstlisting}
\begin{figure}[!htb]
  \centering
  \includegraphics[width=4cm, keepaspectratio]{img/babia_3dchart_example.png}
  \caption{3D bar chart generated with BabiaXR components}
  \label{fig:babia_3d_chart}
\end{figure}

\subsection{BabiaXR Codecity}

The CodeCity version of BabiaXR is developed with the goal of exploring the known limitations of the original CodeCity, the approach is also inside the components pack, specifically, the components related to the CodeCity visualization are \textit{codecity-block} for blocks and \textit{codecity-quarter} for quarters. The first step in order to create this visualization is the layout selection, we rely on the treemap pivot algorithm \cite{ref_pivot} for the blocks and quarters positions, it shows a consistent layout and a desired aspect-ratio of the building area (making them the best "real building" possible appearance).

\begin{figure}[!htb]
  \centering
  \includegraphics[width=9cm, keepaspectratio]{img/babia_codecity_example.png}
  \caption{BabiaXR CodeCity examples}
  \label{fig:babia_gltf}
\end{figure}

Moreover, there is an option for changing the appearance of the buildings for real building models loading gTLF models, making more realistic cities and improving the notion of locality.
As A-Frame extends HTML and each building is defined as an HTML tag, we defined a unique identifier for each building in order to change its area and height in the time evolution maintaining the position and solving one of the limitations that the original CodeCity has. 
In terms of interaction, A-Frame includes by default the possibility of entering the scene in Virtual Reality mode, therefore, the functionality of clicking a quarter with the mouse or a controller if it using a VR device has been implemented in order to show more information about the building/quarter clicked/hovered, showing the name of it as a title on top of it.

\subsection{Creating a City}
We separate the creation of a city corresponding of a software system in two parts, the first one is related of how we retrieve the data that we are going to visualize as a city, we analyze a repository using Graal \cite{ref_graal}, Graal leverages on the Git backend of Perceval \footnote{https://github.com/chaoss/grimoirelab-perceval} and enhances it to set up ad-hoc source code analysis. Thus, it fetches the commits from a Git repository and provides a mechanism to plug third-party tools/libraries focused on source code analysis. Moreover, Graal can retrieve similar metrics as the original CodeCity, we use Graal to obtain analyze projects using code complexity analysis (CoCom) \footnote{https://github.com/chaoss/grimoirelab-graal\#backends}, retrieving metrics like the number of lines of code or the number of functions that the file code has.
Graal stores the data in an ElasticSearch database, once the data is there, we run a Python code included in the BabiaXR components repository in order to get the JSON with the data that will be injected in the CodeCity component of BabiaXR, summarizing:

\begin{enumerate}
    \item Run Graal specifying the ElasticSearch database and the repositories that are going to be analyzed.
    \item Run the code \textit{generate\_structure\_codecityjs.py} with the time evolution flag and the snapshot argument filled and it will return a set of JSON data with all the information needed of the city.
\end{enumerate}

Once we have run the previous code, in the set of JSON files returned, there is one that has the starting information about the city evolution, called \textit{main\_data.json}, it has a structure that defines the data files needed of the time evolution, the sampling days that have been used and the time field where the evolution has been made:
\begin{lstlisting}
{ 
   "date_field":"field",
   "sampling_days":"180",
   "init_data":"data_X_tree.json",
   "time_evolution":true,
   "data_files":[ 
      { 
         "date":1573001804.136289,
         "file":"data_X.json"
      },
      ...
   ]
}
\end{lstlisting}

This file is the starting point and has to be defined as the main data when the component is written in HTML. The data files follow the next structure, starting for the main point, that has all the needed buildings and quarters tree in order to create the city:
\begin{lstlisting}
[{ 
    "block":"name",
    "blocks":[ 
       { 
          "block":"name_child",
          "items":[ 
             { 
                "id":"file_path",
                "area": 1,
                "height":2
             },
             ...
           ]
        },
    ...
}]
\end{lstlisting}

Then, the next files just have a list with the new values of the buildings:
\begin{lstlisting}
[
   {
       "id": "file_path", 
       "name": "file_name", 
       "height": 1, 
       "value": 2
   }, 
...]
\end{lstlisting}

Then, just creating a simple HTML file, loading the dependencies needed (from A-Frame, BabiaXR components and the file \textit{time\_evol.js}), and defining the parameters of the \textit{codecity-quarter} component, we can see the city of the project analyzed evolving among the time:
\begin{lstlisting}
<a-scene id="scene">
    <a-entity codecity-quarter='items: main_data.json' position="0 0 0"></a-entity>
 ...
</a-scene>
\end{lstlisting}

\section{First Applications}

The following figures (\ref{fig:evol_1}, \ref{fig:evol_2}, \ref{fig:evol_3}) represent the analysis of the project Angular \footnote{https://github.com/angular/angular}. Each building represents a file of the project and the quarters are defined as the tree folders that the file belongs to. As metrics, we represent the lines of code as the height of the buildings and the area represents the number of functions that the file has. Moreover, the figures represent a selected time snapshot, starting with the time snapshot of the 5th of November, 2019. Then going back 6 months (the 5th of May, 2019) and going back 18 months (the 5th of May, 2019). The demo available in the reproduction package \footnotemark gives more information because there are more time samples and it is possible to see the evolution of the city in a completed and visual way.

\begin{figure}[!htb]
  \centering
  \includegraphics[width=9cm, keepaspectratio]{img/angular-01.png}
  \caption{Angular CodeCity of the 5th of November, 2019}
  \label{fig:evol_1}
\end{figure}

\begin{figure}[!htb]
  \centering
  \includegraphics[width=9cm, keepaspectratio]{img/angular-02.png}
  \caption{Angular CodeCity of the 5th of May, 2019}
  \label{fig:evol_2}
\end{figure}

\begin{figure}[!htb]
  \centering
  \includegraphics[width=9cm, keepaspectratio]{img/angular-04.png}
  \caption{Angular CodeCity of the 5th of May, 2018}
  \label{fig:evol_3}
\end{figure}

We can observe how the city evolves and how the buildings decrease their area and some of them disappear, as this is an evolution from present to past, some of the files maybe not in the past version, so the space that they filled is changed to empty terrain. For example, focusing on the center of the city, the central quarter is related to some packages that Angular includes in their code, if we observe the evolution, there are some buildings that disappear, those buildings are related to some packages that have been included in Angular in the latest version, their space is not filled in older versions.

There is a reproduction package \footnotemark[\value{footnote}] available in order to reproduce all the steps in a controlled environment.

\footnotetext{https://gitlab.com/thesis-dlumbrer/repr-pckg-benevol-2019}

\section*{Discussion}
The proposed solutions to the known limitations of the original CodeCity has been developed successfully. Using the HTML identifiers of each building fixes the non-fixed location of the buildings, making the same location of each building as the city evolves, just changing the area and the height of each building in each time snapshot. 
In terms of the interaction, the possibility of adding Virtual Reality to the city and the development using web technologies solve the limitation of the interaction of the original CodeCity, making the BabiaXR version more universal and able to use in any device that has a modern web browser that supports WebVR.
On the other hand, we are analyzing similar software metrics of the original CodeCity, it needs to add effort to this research and changes the type of metrics analyzed to new ones that make sense to represent using the city metaphor.

\section*{Conclusion and Future Work}
\label{future_works}

This approach is a proof of concept of a tool that has to evolve. We were focused on replicate CodeCity in a modern and open source environment in order to make it more universal and easy to use, in this case, the web environment, and we were focused on the time evolution feature. The first versions of this tool are just the beginning and it needs more effort on the interaction part, we propose to add more functionality to the user interface, for example, adding two types of cities, one smaller as mock-up in order to see the entire city in a small space and take advantage to the location of the modern VR glasses. And the other one is to make a city as the same "size" as an actual city and tour the user into it, for example, driving a car or even flying. Other future work could be the feature of adding more information about the software in the city, with more user interaction than click/hover.

On the other hand, all the metrics seen were metrics related to modules or packages. Thus, the next step is to look for the right way to represent different kind of data further than software systems structure, using different kinds of metrics. Specifically, we are going to deep in other software development metrics. For instance, the distribution of the commits/repositories, the relationships between the people, the time evolution of the community, etc. Therefore, we have to face the challenge of move this different kind of data to a city, thinking about what will represent the building, the districts, etc. Making the representation easy to understand and easy to answer questions that the common visualizations don't do.

Finally, another research branch is to approach other kinds of metaphor apart from the city, for example, islands, planets, etc.

\section*{Acknowledgment}
This work was supported by the Spanish Government (IND2018/TIC-9669).

\begin{thebibliography}{00}
\bibitem{ref_article3}
R. Wettel and M. Lanza. Visualizing software systems as cities. In \textit{Proceedings of VISSOFT 2007 (4th IEEE International Workshop on Visualizing Software For Understanding and Analysis)} , pages 92–99, 2007

\bibitem{ref_article4}
R. Wettel and M. Lanza. Program comprehension through software habitability. In \textit{Proceedings of 15th International Conference on Program Comprehension (ICPC 2007). IEEE Computer Society}, 2007.

\bibitem{ref_vr_city}
Vincur, J.; Navrat, P. ;Polasek, I. VR City: Software Analysis in Virtual Reality Environment. \textit{2017 IEEE International Conference on Software Quality, Reliability and Security Companion (QRS-C)}, 2017.

\bibitem{ref_vr_city_2}
F. Steinbrückner, C. Lewerentz. Representing development history in software cities, \textit{2010 5th international symposium on Software visualization (SOFTVIS)}, New York, USA, 2010, pp. 193-202.

\bibitem{ref_getaviz}
Baum, D.; Schilbach, J.; Kovacs, P.; Eisenecker, U.; Müller, R. Getaviz: Generating Structural, Behavioral, and Evolutionary Views of Software Systems for Empirical Evaluation. \textit{2017 IEEE Working Conference on Software Visualization (VISSOFT)}, 2017.

\bibitem{ref_article5}
Merino L Ghafari M Anslow C Nierstrasz. CityVR: Gameful Software Visualization. \textit{ International Conference on Software Maintenance and Evolution (ICSME)}, 2017

\bibitem{ref_evocells}
Willy Scheibel; Christopher Weyand; Jürgen Döllner. EvoCells – A Treemap Layout Algorithm for Evolving Tree Data. \textit{9th International Conference on Information Visualization Theory and Applications (IVAPP)}, Madeira, Portugal, 2018

\bibitem{ref_voronoi}
Avneesh Sud; Danyel Fisher; Huai-Ping Lee. Fast Dynamic Voronoi Treemaps. \textit{Seventh International Symposium on Voronoi Diagrams in Science and Engineering, (ISVD 2010)}, Quebec, Canada, June 28-30, 2010

\bibitem{ref_pivot}
Ben Shneiderman M; Ordered Treemaps Layouts. \textit{Information Visualization, 2001. (INFOVIS)}, 2001

\bibitem{ref_graal}
Valerio Cosentino, Santiago Due{\~n}as, Ahmed Zerouali, Gregorio Robles, Jesus M Gonzalez-Barahona. Graal: The Quest for Source Code Knowledge \textit{2018 IEEE 18th International Working Conference on Source Code Analysis and Manipulation (SCAM)}, 2018


\end{thebibliography}


\end{document}
